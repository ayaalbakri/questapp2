﻿**Project Name**


QuestApp2


---


## Project Description

The project built for Clinic aim.


We can Book appointment, Delete, Update, and Show the book details.

---

## Run backend App

1.Install Visual studio


2.The Solution inside QuestApp folder 


3.Run the app


or 


1-Inside QuestApp folder run this command line (dotnet run)


2- The server listen to port : 54185 (Keep in mind this port just for server silde)


---


## Run The App


1.Install Node js


2.Inside front end folder run this command "npm start"


3.The project listen to port 3000 (http://localhost:3000)
---


## Deployment

I deployed my app in Azure (https://questayafrontend.azurewebsites.net/)


